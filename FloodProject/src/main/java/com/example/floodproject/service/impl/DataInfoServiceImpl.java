package com.example.floodproject.service.impl;

import com.example.floodproject.dao.UserTest;
import com.example.floodproject.mapper.DataInfoMapper;
import com.example.floodproject.service.DataInfoService;
import com.example.floodproject.util.GetDataUtil;
import geotrellis.raster.io.geotiff.SinglebandGeoTiff;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.util.HashMap;
import java.util.List;
import java.util.Map;

/*
 * @author 枫城
 * @createTime 2022/9/17 11:42
 * @desc 本类主要处理tiff栅格数据
 */

@Slf4j
@Service
public class DataInfoServiceImpl implements DataInfoService {

    @Autowired
    private DataInfoMapper dataInfoMapper;

    /**
     * 返回extent数据
     * @return map
     */
    @Override
    public Map<String, String> getExtent() {
        final HashMap<String, String> map = new HashMap<>();
        final SinglebandGeoTiff getDataUtil = new GetDataUtil().readData();
        // 打印行列信息
        log.info("行数："+getDataUtil.cols());
        log.info("列数："+getDataUtil.rows());
        // 转成字符
        String extent = getDataUtil.extent().toString();
        log.info(extent);
        // 字符切割
        extent = extent.substring(extent.indexOf("(") + 1 ,extent.indexOf(")"));
        map.put("Extent",extent);
        return map;

    }



    @Override
    public List<UserTest> getAllUsers() {
        return dataInfoMapper.getAllUserInfo();
    }
}
