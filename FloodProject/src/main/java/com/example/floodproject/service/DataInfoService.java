package com.example.floodproject.service;

import com.example.floodproject.dao.UserTest;

import java.util.List;
import java.util.Map;

public interface DataInfoService {

    /**
     * 通过直接读取本地tif文件，而非数据库
     * @return
     * @throws Exception
     */
    Map<String, String> getExtent() throws Exception;

    List<UserTest> getAllUsers();



}
