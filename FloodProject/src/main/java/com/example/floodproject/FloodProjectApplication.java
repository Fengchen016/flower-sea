package com.example.floodproject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FloodProjectApplication {

    public static void main(String[] args) {
        SpringApplication.run(FloodProjectApplication.class, args);
    }

}
