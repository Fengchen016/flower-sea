package com.example.floodproject.mapper;

import com.example.floodproject.dao.UserTest;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface DataInfoMapper {

    /**
     * pg测试
     * @return
     */
    List<UserTest> getAllUserInfo();


}
