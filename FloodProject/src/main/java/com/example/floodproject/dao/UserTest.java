package com.example.floodproject.dao;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/*
 * @author 枫城
 * @createTime 2022/9/19 10:31
 * @desc pg链接测试
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserTest {
    private String name;
    private int id;
    private String sex;
}
