package com.example.floodproject.util

import geotrellis.raster.Tile
import geotrellis.raster.io.geotiff.reader.GeoTiffReader
import geotrellis.raster.io.geotiff.{MultibandGeoTiff, SinglebandGeoTiff}
import geotrellis.spark.{SpatialKey, TileLayerMetadata}

import java.io.{FileNotFoundException, IOException}
import scala.swing.Point

class GetDataUtil{

  // 文件所在地址
  var srcPath : String = "src/main/resources/data/DEM/从江水位站4326.tiff"

  /**
   * 栅格数据快速了解：
   *   栅格数据是由一个个像元（Cell）按照行列方式构成的，每一个像元都有自己的像元值，
   * 并且这些像元值根据栅格类型的不同，可以代表反射值，或高程值，或分类类别等等；
   *
   */

  /**
   * 读取tif文件基本数据信息
   * @return geoTiff
   */
  def readData() ={
    try {
    val geoTiff = SinglebandGeoTiff(srcPath)
      // 直接返回读取的信息，交到java层解析
//      val index = geoTiff.mapXToGrid(x) + geoTiff.mapYToGrid(y) * cols
      geoTiff
    } catch {
      case _: FileNotFoundException =>{
        println("Missing file exception")
        null
      }
      case _: IOException => {
        println("IO Exception")
        null
      }
    }
  }

  def readTestData()={
    val geoTiff: MultibandGeoTiff = GeoTiffReader.readMultiband(srcPath)
    geoTiff
  }


}
