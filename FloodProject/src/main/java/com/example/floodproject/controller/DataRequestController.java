package com.example.floodproject.controller;


import com.example.floodproject.dao.UserTest;
import com.example.floodproject.service.DataInfoService;
import com.example.floodproject.service.impl.DataInfoServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/")
public class DataRequestController {

    @Autowired
    DataInfoService dataInfoService;


    /**
     *
     * @return
     */
    @GetMapping("getExtent")
    public Map<String, String> getExtent() {
        final DataInfoServiceImpl dataInfoService = new DataInfoServiceImpl();
        return dataInfoService.getExtent();
    }

    @GetMapping("getAllUser")
    public List<UserTest>  getAllUsers(){
       return dataInfoService.getAllUsers();
    }

}
