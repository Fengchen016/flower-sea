package com.example.floodproject.test

import geotrellis.raster.io.geotiff.{GeoTiffTile, SinglebandGeoTiff}
import geotrellis.raster.io.geotiff.reader.GeoTiffReader.{GeoTiffInfo, geoTiffSinglebandTile, readSingleband}
import geotrellis.util.{ByteReader, FileRangeReader, StreamingByteReader}

/**
 *
 */
object ScalaDemo2 {

  val path : String = "E:\\东方世纪\\30米DEM数据\\dem.tif"

  def main(args: Array[String]): Unit = {
    val geoTiff = SinglebandGeoTiff(path)
    println(geoTiff.extent, geoTiff.cols, geoTiff.rows)
  }




}
