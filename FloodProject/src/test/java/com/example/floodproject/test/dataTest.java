package com.example.floodproject.test;

import com.example.floodproject.controller.DataRequestController;
import com.example.floodproject.dao.UserTest;
import com.example.floodproject.mapper.DataInfoMapper;
import com.example.floodproject.service.DataInfoService;
import com.example.floodproject.service.impl.DataInfoServiceImpl;
import com.example.floodproject.util.GetDataUtil;
import geotrellis.raster.MultibandTile;
import geotrellis.raster.Raster;
import geotrellis.raster.io.geotiff.MultibandGeoTiff;
import geotrellis.vector.Point;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
public class dataTest {

    String path = "E:\\东方世纪\\30米DEM数据\\dem.tif";

    @Autowired
    DataInfoMapper dataInfoMapper;

    @Autowired
    DataInfoService dataInfoService;


    /**
     * 接口测试getExtent
     */
    @Test
    public void getInfo(){
        final DataInfoServiceImpl data = new DataInfoServiceImpl();
        data.getExtent();

    }

    /**
     * controller测试getExtent、
     */
    @Test
    public void getExtent() {
        final DataRequestController controller = new DataRequestController();
        System.out.println(controller.getExtent());
    }

    /**
     * mapper层测试getAllUserInfo
     */
    @Test
    public void getAllUser(){
        final List<UserTest> allUserInfo = dataInfoMapper.getAllUserInfo();
        for (UserTest userTest : allUserInfo) {
            System.out.println(userTest.toString());
        }
    }

    /**
     * Service层测试getAllUsers
     */
    @Test
    public void getAllUserInfo(){
        final List<UserTest> allUsers = dataInfoService.getAllUsers();
        for (UserTest allUser : allUsers) {
            System.out.println(allUser);
        }
        }

    /**
     * 将得到的数据进行测试
      */
    @Test
    public void tiffDataTest(){
        // 获取到tiff数据
        final MultibandGeoTiff getDataUtil = new GetDataUtil().readTestData();
        // 数据来源于从江，输入x和y为从江的经纬度
        System.out.println(getDataUtil.extent().intersects(108.910425, 25.749379)); // true
        final Raster<MultibandTile> raster = getDataUtil.raster();
//        raster.extract

//        final Point point = new Point(108.910425, 25.749379);
//        final boolean intersects = getDataUtil.extent().intersects(point);// true



    }
}
